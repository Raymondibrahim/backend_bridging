<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MahasiswaSiatma;
use Illuminate\Support\Facades\DB;

class CleanDataMhsController extends Controller
{
    public function index()
    {
        $mhs = DB::connection('sqlsrv')
        ->table('dbo.MHS_LIVE_AKTIF')
        ->select('NPM', 'NAMA_MHS')
        ->where('NAMA_MHS', 'LIKE', '%,%')
        ->distinct('NPM')
        ->get();

        function clean($string) {
            $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
            $string = preg_replace('/[^\da-z ]/i', '', $string); // Removes special chars.
         
            return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
         }

         for ($x = 0; $x < count($mhs); $x++)
        {
            $mhs[$x]->NAMA_MHS = clean($mhs[$x]->NAMA_MHS) ;  
        }   

        if (count($mhs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $mhs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
