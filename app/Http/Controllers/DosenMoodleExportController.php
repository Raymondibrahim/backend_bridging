<?php

namespace App\Http\Controllers;
use App\Exports\DosenMoodleExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class DosenMoodleExportController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function export(Request $request)
    {
        return Excel::download(new DosenMoodleExport, 'ADD Dosen Situs Kuliah.csv');
        return response([
                'message' => 'Download Success',
            ], 200);
    }
}
