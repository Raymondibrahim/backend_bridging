<?php

namespace App\Http\Controllers\Siatma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KrsSiatma;
use Illuminate\Support\Facades\DB;

class KrsSiatmaController extends Controller
{
    //Fungsi untuk mengambil data KRS mahasiswa yang berada pada database SIATMA tabel KRS.
    public function index(Request $request)
    {
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KRS AS tk')
            ->join('dbo.MST_MHS_AKTIF AS mma', 'tk.NPM', '=', 'mma.NPM')
            ->join('dbo.TBL_KELAS AS tkel', 'tk.ID_KELAS', '=', 'tkel.ID_KELAS')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tkel.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            //->join('dbo.MHS_LIVE_AKTIF as mla', 'mma.NPM', '=', 'mla.NPM')
            ->select('tk.ID_KRS', 'tk.NPM', 'mma.NAMA_MHS', 'tk.TGLKRS')
            ->where('tsa.ISCURRENT', '=', '1')
            ->orderBy('tk.ID_KRS')
            ->get();
        }
        else{
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KRS AS tk')
            ->join('dbo.MST_MHS_AKTIF AS mma', 'tk.NPM', '=', 'mma.NPM')
            ->join('dbo.TBL_KELAS AS tkel', 'tk.ID_KELAS', '=', 'tkel.ID_KELAS')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tkel.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            //->join('dbo.MHS_LIVE_AKTIF as mla', 'mma.NPM', '=', 'mla.NPM')
            ->select('tk.ID_KRS', 'tk.NPM', 'mma.NAMA_MHS', 'tk.TGLKRS')
            ->where('tsa.ISCURRENT', '=', '1')
            ->where('tkel.ID_PRODI_BUAT', '=', $storeData['prodi'])
            ->orderBy('tk.ID_KRS')
            ->get();
        }
        

        if (count($krs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $krs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
