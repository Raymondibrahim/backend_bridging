<?php

namespace App\Http\Controllers\Siatma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MahasiswaSiatma;
use Illuminate\Support\Facades\DB;

class SmakSiatmaController extends Controller
{
    //Fungsi untuk mengambil data tahun dan semester akademik yang berada pada database SIATMA.
    public function index()
    {
        $smaksi = DB::connection('sqlsrv')
        ->table('dbo.TBL_SEMESTER_AKADEMIk')
        ->select('ID_TAHUN_AKADEMIK', 'NO_SEMESTER', 'SEMESTER_AKADEMIK')
        ->get();

        if (count($smaksi) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $smaksi,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
