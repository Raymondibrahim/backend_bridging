<?php

namespace App\Http\Controllers\Siatma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MatkulSiatma;
use Illuminate\Support\Facades\DB;

class MatkulSiatmaController extends Controller
{
    //Fungsi untuk mengambil data mata kuliah yang berada pada database SIATMA.
    public function index(Request $request)
    {
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $matkulsi = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS AS tk')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.KODE_MK', 'tk.NAMA_MK', 'tk.SKS', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK', 'tk.NO_SEMESTER')
            ->where('tsa.ISCURRENT', '=', '1')
            ->get();
        }
        else{
            $matkulsi = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS AS tk')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.KODE_MK', 'tk.NAMA_MK', 'tk.SKS', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK', 'tk.NO_SEMESTER')
            ->where('tsa.ISCURRENT', '=', '1')
            ->where('tk.ID_PRODI_BUAT', '=', $storeData['prodi'])
            ->get();
        }

        if (count($matkulsi) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $matkulsi,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
