<?php

namespace App\Http\Controllers\Siatma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MahasiswaSiatma;
use Illuminate\Support\Facades\DB;

class MahasiswaSiatmaController extends Controller
{
    //Fungsi untuk mengambil data mahasiswa yang berada pada database SIATMA tabel MHS_LIVE.
    public function index(Request $request)
    {
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $mhs = DB::connection('sqlsrv')
            ->table('dbo.MST_MHS_AKTIF AS mma')
            ->join('dbo.tbl_induk_mhs AS tim', 'mma.NPM', '=', 'tim.npm')
            ->select('mma.NPM', 'tim.namamhs')
            ->where('KD_STATUS_MHS', '=', 'A')
            ->distinct('mma.NPM')
            ->get();
        }
        else{
            $mhs = DB::connection('sqlsrv')
            ->table('dbo.MST_MHS_AKTIF AS mma')
            ->join('dbo.tbl_induk_mhs AS tim', 'mma.NPM', '=', 'tim.npm')
            ->select('mma.NPM', 'tim.namamhs')
            ->where('KD_STATUS_MHS', '=', 'A')
            ->where('mma.ID_PRODI', '=', $storeData['prodi'])
            ->distinct('mma.NPM')
            ->get();
        }
        

        if (count($mhs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $mhs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
