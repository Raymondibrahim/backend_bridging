<?php

namespace App\Http\Controllers\Siatma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProdiSiatma;
use Illuminate\Support\Facades\DB;

class ProdiSiatmaController extends Controller
{
    //Fungsi untuk mengambil data program studi yang berada pada database SIATMA.
    public function index()
    {
        $prod = DB::connection('sqlsrv')
        ->table('dbo.REF_PRODI AS rp')
        ->join('dbo.REF_FAKULTAS AS rf', 'rp.ID_FAKULTAS', '=', 'rf.ID_FAKULTAS')
        ->select('rp.ID_PRODI', 'rp.PRODI', 'rf.FAKULTAS')
        ->get();

        if (count($prod) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $prod,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
