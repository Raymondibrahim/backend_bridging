<?php

namespace App\Http\Controllers\Siatma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DosenSiatma;
use Illuminate\Support\Facades\DB;

class DosenSiatmaController extends Controller
{
    //Fungsi untuk mengambil data dosen dari database SIMKA
    public function index()
    {
        $dosen = DB::connection('dbsimka')
        ->table('simka.MST_KARYAWAN')
        ->select('NPP', 'NAMA_LENGKAP_GELAR', 'EMAIL_INSTITUSI', 'ALAMAT')
        ->get();

        if (count($dosen) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $dosen,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
