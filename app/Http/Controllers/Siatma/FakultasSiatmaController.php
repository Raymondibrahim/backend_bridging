<?php

namespace App\Http\Controllers\Siatma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FakultasSiatma;
use Illuminate\Support\Facades\DB;

class FakultasSiatmaController extends Controller
{
    //Fungsi untuk mengambil data fakultas dari database SIATMA
    public function index()
    {
        $fakultas = DB::connection('sqlsrv')
        ->table('dbo.REF_FAKULTAS')
        ->select('ID_FAKULTAS', 'FAKULTAS')
        ->get();

        if (count($fakultas) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $fakultas,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
