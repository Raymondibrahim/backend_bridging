<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\MahasiswaMoodleExport;
use Maatwebsite\Excel\Facades\Excel;

class MahasiswaMoodleExportController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    
    public function export(Request $request)
    {
        return Excel::download(new MahasiswaMoodleExport, 'Add MABA Situs Kuliah.csv');
        return response([
                'message' => 'Download Success',
            ], 200);
    }
}
