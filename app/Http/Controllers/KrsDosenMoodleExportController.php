<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\KrsDosenMoodleExport;
use Maatwebsite\Excel\Facades\Excel;

class KrsDosenMoodleExportController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function export(Request $request)
    {
        $tahun = $request->tahun;
        $semester = $request->semester;
        return Excel::download(new KrsDosenMoodleExport($tahun, $semester), 'enroll Dosen.csv');
        return response([
                'message' => 'Download Success',
            ], 200);
    }
}
