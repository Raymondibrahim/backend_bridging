<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DosenBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DosenBaruController extends Controller
{
    //Fungsi untuk mengambil data dosen yang terdapat pada database SIMKA namun tidak ada dalam database MOODLE
    public function index()
    {
        $query = DB::connection('dbmoodle')
                ->table('dosen')
                ->select('idnumber')
                ->get();

        $dosen = DB::connection('dbsimka')
        ->table('simka.MST_KARYAWAN')
        ->select('NPP', 'NAMA_LENGKAP_GELAR', 'EMAIL_INSTITUSI', 'ALAMAT', 'ALAMAT_KOTA')
        ->whereNotIn('NPP', $query = json_decode( json_encode($query), true))
        ->distinct('NPP')
        ->get();

        function clean($string) {
            $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
            $string = preg_replace('/[^\da-z .]/i', '', $string); // Removes special chars.
         
            return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
         }

         for ($x = 0; $x < count($dosen); $x++)
        {
            $dosen[$x]->NAMA_LENGKAP_GELAR = clean($dosen[$x]->NAMA_LENGKAP_GELAR) ;  //Clean data nama mahasiswa yang ada huruf koma (,) di tengah2 nama
        }

        if (count($dosen) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $dosen,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan data yang dicentang pada halaman Dosen Baru di Frontend.
    public function store(Request $request)
    {
        $dosens = json_decode($request->getContent() , true);
            foreach( $dosens as $dsn ){
                $splitName = explode(' ', $dsn['NAMA_LENGKAP_GELAR'], 2);
                if(empty($splitName[1]))
                {
                    $splitName[1] = $splitName[0];
                    $splitName[0] = '';
                }

                $first_name = $splitName[0];
                $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty
                DosenBaru::create([
                    'username' => $dsn['NPP'],
                    'idnumber' => $dsn['NPP'],
                    'firstname' => $first_name,
                    'lastname' => $last_name,
                    'address' => $dsn['ALAMAT'],
                    'auth' => "radius",
                    'email' => $dsn['EMAIL_INSTITUSI'],
                    'city' => $dsn['ALAMAT_KOTA'],
                    'country' => "ID",
                    'lang' => "en",
                    'timezone' => 99,
                    'timecreated' => Carbon::now(+7)->toDateTimeString(),
                    'timemodified' => Carbon::now(+7)->toDateTimeString(),
                ]);
            }
        return response([
            'message' => 'Add Dosen Success',
            'data' => $dosens,
        ],200); //return data katalog baru dalam bentuk json
    }  
}
