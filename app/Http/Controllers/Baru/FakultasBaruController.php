<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FakultasBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class FakultasBaruController extends Controller
{
    //Fungsi untuk menampilkan fakultas yang ada di database SIATMA tapi tidak ada di database MOODLE
    public function index()
    {
        $query = DB::connection('dbmoodle')
                ->table('fakultas')
                ->select('id_fakultas')
                ->get();

        $fakultas = DB::connection('sqlsrv')
        ->table('dbo.REF_FAKULTAS')
        ->select('ID_FAKULTAS', 'FAKULTAS', 'ALAMAT_FAKULTAS', 'ID_FAKULTAS_SIMKA')
        ->whereNotIn('ID_FAKULTAS', $query = json_decode( json_encode($query), true))
        ->distinct('ID_FAKULTAS')
        ->get();

        if (count($fakultas) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $fakultas,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan data fakultas ke dalam database MOODLE.
    public function store(Request $request)
    {
        $fakultas = json_decode($request->getContent() , true);
            foreach( $fakultas as $fak ){
                FakultasBaru::create([
                    'id_fakultas' => $fak['ID_FAKULTAS'],
                    'nama_fakultas' => $fak['FAKULTAS'],
                    'alamat_fakultas' => $fak['ALAMAT_FAKULTAS'],
                    'ID_FAKULTAS_SIMKA' => $fak['ID_FAKULTAS_SIMKA'],
                ]);
            }
        return response([
            'message' => 'Add Prodi Success',
            'data' => $fakultas,
        ],200); //return data katalog baru dalam bentuk json
    }  
}
