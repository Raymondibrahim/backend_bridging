<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProdiBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProdiBaruController extends Controller
{
    //Fungsi untuk mengambil data program studi dari database SIATMA yang tidak ada di dalam database MOODLE.
    public function index()
    {
        $query = DB::connection('dbmoodle')
                ->table('prodi')
                ->select('id_prodi')
                ->get();

        $prodi = DB::connection('sqlsrv')
        ->table('dbo.REF_PRODI')
        ->select('ID_PRODI', 'ID_FAKULTAS', 'PRODI', 'JENJANG')
        ->whereNotIn('ID_PRODI', $query = json_decode( json_encode($query), true))
        ->distinct('ID_PRODI')
        ->get();

        if (count($prodi) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $prodi,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan data program studi yang dicentang pada halaman prodi baru pada frontend.
    public function store(Request $request)
    {
        $prodis = json_decode($request->getContent() , true);
            foreach( $prodis as $pro ){
                ProdiBaru::create([
                    'id_prodi' => $pro['ID_PRODI'],
                    'id_fakultas' => $pro['ID_FAKULTAS'],
                    'nama_prodi' => $pro['PRODI'],
                    'jenjang' => $pro['JENJANG'],
                ]);
            }
        return response([
            'message' => 'Add Prodi Success',
            'data' => $prodis,
        ],200); //return data katalog baru dalam bentuk json
    }  
}
