<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MatkulBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MatkulBaruController extends Controller
{
    //Fungsi untuk menampilkan data mata kuliah yang ada pada database SIATMA namun tidak ada di dalam database MOODLE.
    public function index(Request $request)
    {
        $query = DB::connection('dbmoodle')
                ->table('matakuliah')
                ->select('id_kelas')
                ->get();

        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $matkul = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS as tk')
            ->join('dbo.refsemester as rs', 'tk.NO_SEMESTER', '=', 'rs.idnya')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.ID_KELAS', 'tk.ID_PRODI_BUAT', 'tk.ID_MK', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK',
            'tk.NO_SEMESTER', 'tk.KODE_MK', 'tk.NAMA_MK', 'tk.SKS', 'rs.keterangan')
            ->whereNotIn('ID_KELAS', $query = json_decode( json_encode($query), true))
            ->where('tsa.ISCURRENT', '=', '1')
            ->distinct('ID_KELAS')
            ->get();
        }
        else{
            $matkul = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS as tk')
            ->join('dbo.refsemester as rs', 'tk.NO_SEMESTER', '=', 'rs.idnya')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.ID_KELAS', 'tk.ID_PRODI_BUAT', 'tk.ID_MK', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK',
            'tk.NO_SEMESTER', 'tk.KODE_MK', 'tk.NAMA_MK', 'tk.SKS', 'rs.keterangan')
            ->whereNotIn('ID_KELAS', $query = json_decode( json_encode($query), true))
            ->where('tsa.ISCURRENT', '=', '1')
            ->where('tk.ID_PRODI_BUAT', '=', $storeData['prodi'])
            ->distinct('ID_KELAS')
            ->get();
        }

        if (count($matkul) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $matkul,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan/mengcopy data mata kuliah yang dicentang pada halaman Mata Kuliah Baru pada frontend ke dalam database MOODLE.
    public function store(Request $request)
    {
        $matkuls = json_decode($request->getContent() , true);
            foreach( $matkuls as $mat ){

                $kodethn = $mat['ID_TAHUN_AKADEMIK'];
                $kodethn = substr( $kodethn, -2);
                $kodesmst = $mat['NO_SEMESTER'];
                $kodethnajaran = $kodethn . '' . $kodesmst;

                // $smesterakdemik = DB::connection('sqlsrv')
                // ->table('dbo.TBL_SEMESTER_AKADEMIK')
                // ->select('SEMESTER_AKADEMIk')
                // ->where('ID_TAHUN_AKADEMIK', '=', $mat['ID_TAHUN_AKADEMIK'])
                // ->where('NO_SEMESTER', '=', $mat['NO_SEMESTER'])
                // ->get()
                // ->first()->SEMESTER_AKADEMIk;

                $shortname = $kodethnajaran . '-' . $mat['ID_PRODI_BUAT'] . '-' . $mat['KODE_MK'] . '-' . $mat['KELAS'];

                $fullname = $mat['NAMA_MK'] . ' ' . $mat['KELAS'] . ' (' . $mat['keterangan'] . ' TA ' . $mat['ID_TAHUN_AKADEMIK'] . '/' . $mat['ID_TAHUN_AKADEMIK']+1 . ')' ;


                $prodi = DB::connection('sqlsrv')
                ->table('dbo.REF_PRODI AS rp')
                ->join('dbo.REF_FAKULTAS AS rf', 'rf.ID_FAKULTAS', '=', 'rp.ID_FAKULTAS')
                ->select('rp.PRODI', 'rf.FAKULTAS')
                ->where('ID_PRODI', '=', $mat['ID_PRODI_BUAT'])
                ->get();

                $categorypath = 'Fakultas ' . $prodi[0]->FAKULTAS . ' / ' . 'Program Studi ' . $prodi[0]->PRODI . ' / ' .  $mat['keterangan'] . ' TA ' . $mat['ID_TAHUN_AKADEMIK'] . '-' . $mat['ID_TAHUN_AKADEMIK']+1;

                MatkulBaru::create([
                    'id_kelas' => $mat['ID_KELAS'],
                    'shortname' => $shortname,
                    'fullname' => $fullname,
                    'category_path' => $categorypath,
                    'idnumber' => $shortname,
                    'format' => 'topics',
                    'summary' => $fullname,
                    'id_prodi' => $mat['ID_PRODI_BUAT'],
                ]);
            }
        return response([
            'message' => 'Add Mata Kuliah Success',
            'data' => $matkuls,
        ],200); //return data katalog baru dalam bentuk json
    }  
}
