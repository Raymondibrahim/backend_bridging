<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MahasiswaBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MahasiswaBaruController extends Controller
{
    //Fungsi untuk mengambil data mahasiswa yang ada pada database SIATMA namun tidak ada di dalam database MOODLE.
    public function index(Request $request)
    {
        $templatemail = "@students.uajy.ac.id";

        $query = DB::connection('dbmoodle')
                ->table('kuliah_user')
                ->select('idnumber')
                ->get();
        
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $mhs = DB::connection('sqlsrv')
            ->table('dbo.MST_MHS_AKTIF AS mma')
            ->join('dbo.tbl_induk_mhs AS tim', 'mma.NPM', '=', 'tim.npm')
            ->select('mma.NPM', 'tim.namamhs', 'tim.almtjogja', 
            DB::raw("CONCAT(mma.NPM, '', '$templatemail') AS email"))
            ->where('mma.NPM', 'not like', '%/P%')
            ->where('mma.NPM', 'not like', '%A')
            ->whereNotIn('mma.NPM', $query = json_decode( json_encode($query), true))
            ->where('mma.KD_STATUS_MHS', '=', 'A')
            ->distinct('mma.NPM')
            ->get();
        }
        else{
            $mhs = DB::connection('sqlsrv')
            ->table('dbo.MST_MHS_AKTIF AS mma')
            ->join('dbo.tbl_induk_mhs AS tim', 'mma.NPM', '=', 'tim.npm')
            ->select('mma.NPM', 'tim.namamhs', 'tim.almtjogja', 
            DB::raw("CONCAT(mma.NPM, '', '$templatemail') AS email"))
            ->where('mma.NPM', 'not like', '%/P%')
            ->where('mma.NPM', 'not like', '%A')
            ->whereNotIn('mma.NPM', $query = json_decode( json_encode($query), true))
            ->where('mma.KD_STATUS_MHS', '=', 'A')
            ->where('mma.ID_PRODI', '=', $storeData['prodi'])
            ->distinct('mma.NPM')
            ->get();
        }

        
        //Fungsi untuk menghilangkan tanda koma dalam tengah2 nama Contohnya Lim, Surya Aditya. Tanda koma dihilangkan karena akan dieksport ke dalam excel csv comma delimitted.
        function clean($string) {
            $string = str_replace(' ', ' ', $string); // Replaces all spaces with hyphens.
            $string = preg_replace('/[^\da-z .]/i', '', $string); // Removes special chars.
         
            return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
         }

         for ($x = 0; $x < count($mhs); $x++)
        {
            $mhs[$x]->namamhs = clean($mhs[$x]->namamhs) ;  //Clean data nama mahasiswa yang ada huruf koma (,) di tengah2 nama
        }   

        if (count($mhs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $mhs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan data yang dicentang pada frontend ke dalam database MOODLE
    public function store(Request $request)
    {
        $mahasiswas = json_decode($request->getContent() , true);
            foreach( $mahasiswas as $mhs ){
                $splitName = explode(' ', $mhs['namamhs'], 2);
                if(empty($splitName[1]))
                {
                    $splitName[1] = $splitName[0];
                    $splitName[0] = '';
                }

                $first_name = $splitName[0];
                $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // Apabila tidak memiliki last name, maka akan dibuat kosong.
                MahasiswaBaru::create([
                    'username' => $mhs['NPM'],
                    'idnumber' => $mhs['NPM'],
                    'firstname' => $first_name,
                    'lastname' => $last_name,
                    'address' => $mhs['almtjogja'],
                    'auth' => "radius",
                    'email' => $mhs['email'],
                    'city' => "Yogyakarta",
                    'country' => "ID",
                    'lang' => "en",
                    'timezone' => 99,
                    'timecreated' => Carbon::now(+7)->toDateTimeString(),
                    'timemodified' => Carbon::now(+7)->toDateTimeString(),
                    'confirmed' => 1,  
                ]);
            }
        return response([
            'message' => 'Add mahasiswa Success',
            'data' => $mahasiswas,
        ],200); //return data katalog baru dalam bentuk json
    }  
    
}
