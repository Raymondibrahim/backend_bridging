<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KrsDosenBaru;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class KrsDosenBaruController extends Controller
{
    //Fungsi untuk mengambil data dari Enroll Dosen dari database SIATMA tabel Mata Kuliah.
    //Data yang diambil adalah data yang berada pada database SIATMA namun tidak ada dalam database MOODLE.
    public function index(Request $request)
    {
        $query = DB::connection('dbmoodle')
                ->table('enroll_krs_dosen')
                ->select('id_kelas')
                ->get();
    
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS as tk')
            ->join('dbo.refsemester as rs', 'tk.NO_SEMESTER', '=', 'rs.idnya')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.ID_KELAS', 'tk.ID_MK', 'tk.ID_PRODI_BUAT', 'tk.KODE_MK', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK',
            'tk.NO_SEMESTER', 'tk.KODE_MK', 'tk.NAMA_MK', 'tk.NAMA_MK_ENG', 'tk.SKS', 'rs.keterangan', 'tk.NPP_DOSEN1', 'tk.NPP_DOSEN2', 
            'tk.NPP_DOSEN3', 'tk.NPP_DOSEN4')
            ->whereNotIn('tk.ID_KELAS', $query = json_decode( json_encode($query), true))
            ->where('tsa.ISCURRENT', '=', '1')
            ->distinct('tk.ID_KELAS')
            ->get();
        }
        else{
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KELAS as tk')
            ->join('dbo.refsemester as rs', 'tk.NO_SEMESTER', '=', 'rs.idnya')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'tk.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('tk.ID_KELAS', 'tk.ID_MK', 'tk.ID_PRODI_BUAT', 'tk.KODE_MK', 'tk.KELAS', 'tk.ID_TAHUN_AKADEMIK',
            'tk.NO_SEMESTER', 'tk.KODE_MK', 'tk.NAMA_MK', 'tk.NAMA_MK_ENG', 'tk.SKS', 'rs.keterangan', 'tk.NPP_DOSEN1', 'tk.NPP_DOSEN2', 
            'tk.NPP_DOSEN3', 'tk.NPP_DOSEN4')
            ->whereNotIn('tk.ID_KELAS', $query = json_decode( json_encode($query), true))
            ->where('tk.ID_PRODI_BUAT', '=', $storeData['prodi'])
            ->where('tsa.ISCURRENT', '=', '1')
            ->distinct('tk.ID_KELAS')
            ->get();
        }
        

        if (count($krs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $krs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan data (mengcopy data) dari database SIATMA ke database MOODLE
    public function store(Request $request)
    {
        $krses = json_decode($request->getContent() , true);
            foreach( $krses as $krs ){

                $kodethn = $krs['ID_TAHUN_AKADEMIK'];
                $kodethn = substr( $kodethn, -2);
                $kodesmst = $krs['NO_SEMESTER'];
                $kodethnajaran = $kodethn . '' . $kodesmst;

                //Fungsi untuk membuat kode akademik Cth: Gasal TA 2021/2022
                $kodeakademik = $krs['keterangan'] . ' TA ' . $krs['ID_TAHUN_AKADEMIK'] . '/' . $krs['ID_TAHUN_AKADEMIK']+1 ;

                //Fungsi untuk membuat kode shortname Cth: 211-02-SIPL33504-B
                //21 menandakan tahun 2021, 1 menandakan semester gasal, 02 menandakan id_prodi, SIPL33504 menandakan kode mata kuliah, B menandakan kelasnya.
                $shortname = $kodethnajaran . '-' . $krs['ID_PRODI_BUAT'] . '-' . $krs['KODE_MK'] . '-' . $krs['KELAS'];

                KrsDosenBaru::create([
                    'id_kelas' => $krs['ID_KELAS'],
                    'npp' => $krs['NPP_DOSEN1'],
                    'fullname' => $this->NamaDosen($krs['NPP_DOSEN1']),
                    'shortname' => $shortname,
                    'kode_mk' => $krs['KODE_MK'],
                    'nama_mk' => $krs['NAMA_MK'],
                    'nama_mk_eng' => $krs['NAMA_MK_ENG'],
                    'kelas' => $krs['KELAS'],
                    'id_prodi' => $krs['ID_PRODI_BUAT'],
                    'smak' => $kodethnajaran,
                    'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                    'semester' => $krs['NO_SEMESTER'],
                    'npp_dosen1' => $krs['NPP_DOSEN1'],
                    'npp_dosen2' => $krs['NPP_DOSEN2'],
                    'npp_dosen3' => $krs['NPP_DOSEN3'],
                    'npp_dosen4' => $krs['NPP_DOSEN4'],
                    'aka' => $kodeakademik,
                    'sks' => $krs['SKS'],
                ]);
                if(!is_null($krs['NPP_DOSEN2'])){
                    KrsDosenBaru::create([
                        'id_kelas' => $krs['ID_KELAS'],
                        'npp' => $krs['NPP_DOSEN2'],
                        'fullname' => $this->NamaDosen($krs['NPP_DOSEN2']),
                        'shortname' => $shortname,
                        'kode_mk' => $krs['KODE_MK'],
                        'nama_mk' => $krs['NAMA_MK'],
                        'nama_mk_eng' => $krs['NAMA_MK_ENG'],
                        'kelas' => $krs['KELAS'],
                        'id_prodi' => $krs['ID_PRODI_BUAT'],
                        'smak' => $kodethnajaran,
                        'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                        'semester' => $krs['NO_SEMESTER'],
                        'npp_dosen1' => $krs['NPP_DOSEN1'],
                        'npp_dosen2' => $krs['NPP_DOSEN2'],
                        'npp_dosen3' => $krs['NPP_DOSEN3'],
                        'npp_dosen4' => $krs['NPP_DOSEN4'],
                        'aka' => $kodeakademik,
                        'sks' => $krs['SKS'],
                    ]);
                }

                if(!is_null($krs['NPP_DOSEN3'])){
                    KrsDosenBaru::create([
                        'id_kelas' => $krs['ID_KELAS'],
                        'npp' => $krs['NPP_DOSEN3'],
                        'fullname' => $this->NamaDosen($krs['NPP_DOSEN3']),
                        'shortname' => $shortname,
                        'kode_mk' => $krs['KODE_MK'],
                        'nama_mk' => $krs['NAMA_MK'],
                        'nama_mk_eng' => $krs['NAMA_MK_ENG'],
                        'kelas' => $krs['KELAS'],
                        'id_prodi' => $krs['ID_PRODI_BUAT'],
                        'smak' => $kodethnajaran,
                        'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                        'semester' => $krs['NO_SEMESTER'],
                        'npp_dosen1' => $krs['NPP_DOSEN1'],
                        'npp_dosen2' => $krs['NPP_DOSEN2'],
                        'npp_dosen3' => $krs['NPP_DOSEN3'],
                        'npp_dosen4' => $krs['NPP_DOSEN4'],
                        'aka' => $kodeakademik,
                        'sks' => $krs['SKS'],
                    ]);
                }

                if(!is_null($krs['NPP_DOSEN4'])){
                    KrsDosenBaru::create([
                        'id_kelas' => $krs['ID_KELAS'],
                        'npp' => $krs['NPP_DOSEN4'],
                        'fullname' => $this->NamaDosen($krs['NPP_DOSEN4']),
                        'shortname' => $shortname,
                        'kode_mk' => $krs['KODE_MK'],
                        'nama_mk' => $krs['NAMA_MK'],
                        'nama_mk_eng' => $krs['NAMA_MK_ENG'],
                        'kelas' => $krs['KELAS'],
                        'id_prodi' => $krs['ID_PRODI_BUAT'],
                        'smak' => $kodethnajaran,
                        'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                        'semester' => $krs['NO_SEMESTER'],
                        'npp_dosen1' => $krs['NPP_DOSEN1'],
                        'npp_dosen2' => $krs['NPP_DOSEN2'],
                        'npp_dosen3' => $krs['NPP_DOSEN3'],
                        'npp_dosen4' => $krs['NPP_DOSEN4'],
                        'aka' => $kodeakademik,
                        'sks' => $krs['SKS'],
                    ]);
                }
            }

        return response([
            'message' => 'Add Data KRS Success',
            'data' => $krses,
        ],200); //return data krs baru dalam bentuk json
    }

    //Fungsi untuk mengambil nama dosen berdasarkan NPP-nya
    private function NamaDosen($npp)
    {
                return DB::connection('dbsimka')
                    ->table('simka.MST_KARYAWAN')
                    ->select('NAMA_LENGKAP_GELAR')
                    ->where('NPP', '=', $npp)
                    ->pluck('NAMA_LENGKAP_GELAR')
                    ->first();
    }

    //Fungsi dalam controller untuk melakukan input dosen manual (biasanya menambahkan seorang mahasiswa sebagai asisten dosen)
    public function insert(Request $request)
    {
        $krs = $request->all();
        $validate = Validator::make($krs, [
            'ID_KELAS' => 'required',
            'ID_PRODI_BUAT' => 'required',
            'ID_TAHUN_AKADEMIK' => 'required',
            'NO_SEMESTER' => 'required',
            'keterangan' => 'required',
            'KODE_MK' => 'required',
            'KELAS' => 'required',
            'NAMA_MK' => 'required',
            'SKS' => 'required',
            'NPP_DOSEN1' => 'required',
        ]); //membuat rule validasi input

        if ($validate->fails())
            return response(['message' => $validate->errors()], 400); //return error invalid input

                $kodethn = $krs['ID_TAHUN_AKADEMIK'];
                $kodethn = substr( $kodethn, -2);
                $kodesmst = $krs['NO_SEMESTER'];
                $kodethnajaran = $kodethn . '' . $kodesmst;

                $kodeakademik = $krs['keterangan'] . ' TA ' . $krs['ID_TAHUN_AKADEMIK'] . '/' . $krs['ID_TAHUN_AKADEMIK']+1 ;

                $shortname = $kodethnajaran . '-' . $krs['ID_PRODI_BUAT'] . '-' . $krs['KODE_MK'] . '-' . $krs['KELAS'];

                KrsDosenBaru::create([
                    'id_kelas' => $krs['ID_KELAS'],
                    'npp' => $krs['NPP_DOSEN1'],
                    'shortname' => $shortname,
                    'kode_mk' => $krs['KODE_MK'],
                    'nama_mk' => $krs['NAMA_MK'],
                    'nama_mk_eng' => $krs['NAMA_MK'],
                    'kelas' => $krs['KELAS'],
                    'id_prodi' => $krs['ID_PRODI_BUAT'],
                    'smak' => $kodethnajaran,
                    'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                    'semester' => $krs['NO_SEMESTER'],
                    'npp_dosen1' => $krs['NPP_DOSEN1'],
                    'npp_dosen2' => null,
                    'npp_dosen3' => null,
                    'npp_dosen4' => null,
                    'aka' => $kodeakademik,
                    'sks' => $krs['SKS'],
                ]);
            
        return response([
            'message' => 'Add Data KRS Success',
            'data' => $krs,
        ],200); //return data krs baru dalam bentuk json
    }  
}
