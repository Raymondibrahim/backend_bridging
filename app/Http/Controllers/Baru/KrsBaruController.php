<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\KrsBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class KrsBaruController extends Controller
{
    //Fungsi untuk menampilkan data KRS Mahasiswa yang ada pada database SIATMA namun tidak ada di database MOODLE.
    public function index(Request $request)
    {
        $query = DB::connection('dbmoodle')
        ->table('enroll_krs_student')
        ->select('id_krs')
        ->get();

        $storeData = $request->all();

        if($storeData['prodi'] == 'admin'){
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KRS as krs')
            ->join('dbo.TBL_KELAS as kls', 'krs.ID_KELAS', '=', 'kls.ID_KELAS')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'kls.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('krs.ID_KRS', 'krs.NPM', 'krs.TGLKRS', 'kls.ID_PRODI_BUAT', 'kls.KODE_MK',
            'kls.KELAS', 'kls.NO_SEMESTER', 'kls.ID_TAHUN_AKADEMIK')
            ->whereNotIn('krs.ID_KRS', $query = json_decode( json_encode($query), true))
            ->where('tsa.ISCURRENT', '=', '1')
            ->distinct('krs.ID_KRS')
            ->get();
        }
        else{
            $krs = DB::connection('sqlsrv')
            ->table('dbo.TBL_KRS as krs')
            ->join('dbo.TBL_KELAS as kls', 'krs.ID_KELAS', '=', 'kls.ID_KELAS')
            ->join('dbo.TBL_SEMESTER_AKADEMIK as tsa', 'kls.ID_TAHUN_AKADEMIK', '=', 'tsa.ID_TAHUN_AKADEMIK')
            ->select('krs.ID_KRS', 'krs.NPM', 'krs.TGLKRS', 'kls.ID_PRODI_BUAT', 'kls.KODE_MK',
            'kls.KELAS', 'kls.NO_SEMESTER', 'kls.ID_TAHUN_AKADEMIK')
            ->whereNotIn('krs.ID_KRS', $query = json_decode( json_encode($query), true))
            ->where('tsa.ISCURRENT', '=', '1')
            ->where('kls.ID_PRODI_BUAT', '=', $storeData['prodi'])
            ->distinct('krs.ID_KRS')
            ->get();
        }

        if (count($krs) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $krs,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan/mengcopy data KRS mahasiswa dari database SIATMA ke database MOODLE
    public function store(Request $request)
    {
        $krses = json_decode($request->getContent() , true);
            foreach( $krses as $krs ){

                $kodethn = $krs['ID_TAHUN_AKADEMIK'];
                $kodethn = substr( $kodethn, -2);
                $kodesmst = $krs['NO_SEMESTER'];
                $kodethnajaran = $kodethn . '' . $kodesmst;

                $shortname = $kodethnajaran . '-' . $krs['ID_PRODI_BUAT'] . '-' . $krs['KODE_MK'] . '-' . $krs['KELAS'];

                KrsBaru::create([
                    'id_krs' => $krs['ID_KRS'],
                    'npm' => $krs['NPM'],
                    'shortname' => $shortname,
                    'tglkrs' => $krs['TGLKRS'],
                    'kode_mk' => $krs['KODE_MK'],
                    'kelas' => $krs['KELAS'],
                    'id_prodi' => $krs['ID_PRODI_BUAT'],
                    'smak' => $kodethnajaran,
                    'tahun_akademik' => $krs['ID_TAHUN_AKADEMIK'],
                    'semester' => $krs['NO_SEMESTER'],
                ]);
            }
        return response([
            'message' => 'Add Data KRS Success',
            'data' => $krses,
        ],200); //return data krs baru dalam bentuk json
    }  
}
