<?php

namespace App\Http\Controllers\Baru;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SmakBaru;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SmakBaruController extends Controller
{
    //Fungsi untuk mengambil data semester akademik (tahun dan semester) dari database SIATMA yang tidak ada pada database MOODLE.
    public function index()
    {
        $query = DB::connection('dbmoodle')
                    ->table('smak')
                    ->select('SEMESTER_AKADEMIk')
                    ->get();

        $smak = DB::connection('sqlsrv')
        ->table('dbo.TBL_SEMESTER_AKADEMIK')
        ->select('ID_TAHUN_AKADEMIK', 'NO_SEMESTER', 'SEMESTER_AKADEMIk', 'ISCURRENT')
        ->whereNotIn('SEMESTER_AKADEMIk', $query = json_decode( json_encode($query), true))
        ->distinct('SEMESTER_AKADEMIk')
        ->get();

        if (count($smak) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $smak,
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }

    //Fungsi untuk memasukkan/mengcopy data semester akademik dari database SIATMA ke database MOODLE.
    public function store(Request $request)
    {
        $smaks = json_decode($request->getContent() , true);
            foreach( $smaks as $smak ){
                SmakBaru::create([
                    'ID_TAHUN_AKADEMIK' => $smak['ID_TAHUN_AKADEMIK'],
                    'NO_SEMESTER' => $smak['NO_SEMESTER'],
                    'SEMESTER_AKADEMIK' => $smak['SEMESTER_AKADEMIk'],
                    'ISCURRENT' => $smak['ISCURRENT'],
                ]);
            }
        return response([
            'message' => 'Add Semester Akademik Success',
            'data' => $smaks,
        ],200); //return data katalog baru dalam bentuk json
    }  
}
