<?php

namespace App\Http\Controllers\Moodle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatkulMoodleController extends Controller
{
    //Fungsi untuk mengambil data mata kuliah pada database MOODLE.
    public function index(Request $request)
    {
        $storeData = $request->all();
        if($storeData['prodi'] == 'admin'){
            $query = DB::connection('dbmoodle')
                    ->table('matakuliah')
                    ->get();
        }else{
            $query = DB::connection('dbmoodle')
                    ->table('matakuliah')
                    ->where('id_prodi', '=', $storeData['prodi'])
                    ->get();
        }

            if (count($query) > 0) {
                return response([
                    'message' => 'Retrieve All Success',
                    'data' => $query,
                ], 200);
            }

            return response([
                'message' => 'Empty',
                'data' => null,
            ], 404);
    }
}
