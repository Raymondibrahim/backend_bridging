<?php

namespace App\Http\Controllers\Moodle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KrsDosenMoodleController extends Controller
{
    //Fungsi untuk mengambil data Enroll dosen (KRS Dosen) dari database MOODLE
    public function index(Request $request)
    {
        $storeData = $request->all();
        
        $query2 = DB::connection('dbmoodle')
        ->table('enroll_krs_dosen')
        ->select('tahun_akademik')
        ->distinct('tahun_akademik')
        ->orderBy('tahun_akademik')
        ->get();

        $query3 = DB::connection('dbmoodle')
        ->table('enroll_krs_dosen')
        ->select('semester')
        ->distinct('semester')
        ->orderBy('semester')
        ->get();

        if($storeData['prodi'] == 'admin'){
            $query = DB::connection('dbmoodle')
                ->table('enroll_krs_dosen')
                ->get();
        }else{
            $query = DB::connection('dbmoodle')
                ->table('enroll_krs_dosen')
                ->where('id_prodi', '=', $storeData['prodi'])
                ->get();
        }

        if (count($query) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $query,
                'tahun' => $query2,
                'semester' => $query3
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
