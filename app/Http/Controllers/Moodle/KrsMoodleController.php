<?php

namespace App\Http\Controllers\Moodle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KrsMoodleController extends Controller
{
    //Fungsi untuk mengambil data KRS Mahasiswa dari database MOODLE
    public function index()
    {
        $query = DB::connection('dbmoodle')
                ->table('enroll_krs_student')
                ->get();

        $query2 = DB::connection('dbmoodle')
        ->table('enroll_krs_student')
        ->select('tahun_akademik')
        ->distinct('tahun_akademik')
        ->orderBy('tahun_akademik')
        ->get();

        $query3 = DB::connection('dbmoodle')
        ->table('enroll_krs_student')
        ->select('semester')
        ->distinct('semester')
        ->orderBy('semester')
        ->get();

        if (count($query) > 0) {
            return response([
                'message' => 'Retrieve All Success',
                'data' => $query,
                'tahun' => $query2,
                'semester' => $query3
            ], 200);
        }

        return response([
            'message' => 'Empty',
            'data' => null,
        ], 404);
    }
}
