<?php

namespace App\Http\Controllers\Moodle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaMoodleController extends Controller
{
    //Fungsi untuk mengambil data mahasiswa yang ada pada database MOODLE.
    public function index()
    {
        $query = DB::connection('dbmoodle')
                    ->table('kuliah_user')
                    ->select('*', DB::raw("CONCAT(firstname,' ',lastname) AS full_name"))
                    ->get();

            if (count($query) > 0) {
                return response([
                    'message' => 'Retrieve All Success',
                    'data' => $query,
                ], 200);
            }

            return response([
                'message' => 'Empty',
                'data' => null,
            ], 404);
    }
}
