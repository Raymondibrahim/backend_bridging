<?php

namespace App\Http\Controllers\Moodle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\DosenBaru;

class DosenMoodleController extends Controller
{
    //Fungsi untuk mengambil data dosen yang ada pada database MOODLE
    public function index()
    {
        $query = DB::connection('dbmoodle')
                    ->table('dosen')
                    ->select('*', DB::raw("CONCAT(firstname,' ',lastname) AS full_name"))
                    ->get();

            if (count($query) > 0) {
                return response([
                    'message' => 'Retrieve All Success',
                    'data' => $query,
                ], 200);
            }

            return response([
                'message' => 'Empty',
                'data' => null,
            ], 404);
    }

    //method untuk mengubah email
    public function update(Request $request, $idnumber){
        $data = DosenBaru::find($idnumber); //mencari data dosen berdasarkan id
        if(is_null($data)){
            return response([
                'message' => 'Dosen Not Found',
                'data' => null
            ],404);
        } //return message saat data dosen tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'email' => 'required'
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input

        $data->email = $updateData['email'];

        if($data->save()){
            return response([
                'message' => 'Update email Success',
                'data' => $data,
            ],200);
        } //return data email yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update email Failed',
            'data' => null,
        ],400); //return message saat email gagal di edit
    }
}
