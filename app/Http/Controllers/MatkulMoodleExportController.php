<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\MatkulMoodleExport;
use Maatwebsite\Excel\Facades\Excel;

class MatkulMoodleExportController extends Controller
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    
    public function export(Request $request)
    {
        $startdate = $request->startdate;
        $enddate = $request->enddate;
        return Excel::download(new MatkulMoodleExport($startdate, $enddate), 'Enroll MK.csv');
        return response([
                'message' => 'Download Success',
            ], 200);
    }
}
