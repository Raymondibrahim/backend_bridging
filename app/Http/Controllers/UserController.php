<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator; //import library untuk validasi
use App\Models\UserBridging; //import model user
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index(){
        $users = DB::connection('dbmoodle')
        ->table('users')
        ->get();
        
        $unit = DB::connection('dbmoodle')
        ->table('prodi')
        ->select('id_prodi', 'nama_prodi')
        ->get();

        if(count($users) > 0){
            return response([
                'message' => 'Retrieve All Success',
                'data' => $users,
                'unit' => $unit,
            ],200);
        } //return data semua user dalam bentuk json

        return response([
            'message' => 'Empty',
            'data' => null
        ],404); //return message data user kosong
    }

    //method untuk menampilkan 1 data user (search)
    public function show($id){
        $user = UserBridging::find($id); //mencari data user berdasarkan id

        if(!is_null($user)){
            return response([
                'message' => 'Retrieve user Success',
                'data' => $user
            ],200);
        } //return data user yang ditemukan dalam bentuk json

        return response([
            'message' => 'user Not Found',
            'data' => null
        ],404); //return message saat data user tidak ditemukan
    }

    //method untuk menambah 1 data user baru (create)
    public function store(Request $request){
        $storeData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($storeData, [
            'name' => 'required|max:60',
            'prodi' => 'required',
            'username' => 'required',
            'password' => 'required',
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input

        $storeData['password'] = bcrypt($request->password); //enkripsi password
        $user = UserBridging::create($storeData); //menambah data user baru
        return response([
            'message' => 'Add user Success',
            'data' => $user,
        ],200); //return data user baru dalam bentuk json
    }

    public function destroy($id){
        $user = UserBridging::find($id); //mencari data user berdasarkan id

        if(is_null($user)){
            return response([
                'message' => 'user Not Found',
                'data' => null
            ],404);
        } //return message saat data user tidak ditemukan

        if($user->delete()){
            return response([
                'message' => 'Delete user Success',
                'data' => $user,
            ],200);
        } //return message saat berhasil menghapus data user
        return response([
            'message' => 'Delete user Failed',
            'data' => null,
        ],400); //return message saat gagal menghapus data user
    }

    public function update(Request $request, $id){
        $user = UserBridging::find($id); //mencari data user berdasarkan id
        if(is_null($user)){
            return response([
                'message' => 'user Not Found',
                'data' => null
            ],404);
        } //return message saat data user tidak ditemukan

        $updateData = $request->all(); //mengambil semua input dari api client
        $validate = Validator::make($updateData, [
            'username' => '',
            'password' => '',
        ]); //membuat rule validasi input

        if($validate->fails())
            return response(['message' => $validate->errors()],400); //return error invalid input

        $updateData['password'] = bcrypt($request->password); //enkripsi password
        $user->username = $updateData['username']; //edit username untuk login
        $user->password = $updateData['password']; //edit password untuk login
        $user->updated_at = Carbon::now(+7)->toDateTimeString();

        if($user->save()){
            return response([
                'message' => 'Update user Success',
                'data' => $user,
            ],200);
        } //return data user yang telah di edit dalam bentuk json
        return response([
            'message' => 'Update user Failed',
            'data' => null,
        ],400); //return message saat user gagal di edit
    }

}
