<?php

namespace App\Exports;

use App\Models\MatkulMoodle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class MatkulMoodleExport implements FromQuery, WithHeadings, WithStrictNullComparison, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    protected $startdate;
    protected $enddate;

    function __construct($startdate, $enddate) {
        $this->startdate = $startdate;
        $this->enddate = $enddate;
    }

    public function query()
        {
            $data = DB::connection('dbmoodle')
            ->table('matakuliah')
            ->select('shortname', 'fullname', 'category_path', 'idnumber', 'format', 'summary')
            ->orderBy('id_kelas');

            return $data;
        }
    
        public function map($data): array
    {
        return [
            $data->shortname,
            $data->fullname,
            $data->category_path,
            $data->idnumber,
            $data->format,
            $this->startdate,
            $this->enddate,
            $data->summary,
        ];
    }

    public function registerEvents(): array
        {
            return [
                AfterSheet::class    => function(AfterSheet $event) {
                    $cellRange = 'A1:W1'; // All headers
                },
            ];
        }

    public function headings(): array
        {

            return [
                [
                    'shortname',
                    'fullname',
                    'category_path',
                    'idnumber',
                    'format',
                    'startdate',
                    'enddate',
                    'summary'
                ]
            ];
        }
}
