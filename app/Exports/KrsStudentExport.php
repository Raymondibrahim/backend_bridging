<?php

namespace App\Exports;

use App\Models\KrsStudentMoodle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class KrsStudentExport implements FromQuery, WithStrictNullComparison, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    protected $tahun;
    protected $semester;

    function __construct($tahun, $semester) {
        $this->tahun = $tahun;
        $this->semester = $semester;
    }
    
    public function query()
        {
            $data = DB::connection('dbmoodle')
            ->table('enroll_krs_student')
            ->select('input_type', 'type', 'npm', 'shortname')
            ->where('tahun_akademik', '=', $this->tahun)
            ->where('semester', '=', $this->semester)
            ->orderBy('npm');

            return $data;
            // return KrsStudentMoodle::selectRaw("input_type, type, npm, shortname, 
            // null as column_1, null as column_2, npm, kode_mk, kelas, id_prodi");
        }

    public function registerEvents(): array
        {
            return [
                AfterSheet::class    => function(AfterSheet $event) {
                    $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(30);
                    $event->sheet->getDelegate()->getStyle('E1:J1')
                                ->getFont()
                                ->setBold(true);
                    $event->sheet->getDelegate()->getStyle('A1:J1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getStyle('A1:D7000')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                },
            ];
        }
}
