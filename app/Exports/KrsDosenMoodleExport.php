<?php

namespace App\Exports;

use App\Models\KrsDosenMoodle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class KrsDosenMoodleExport implements FromQuery, WithStrictNullComparison, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    protected $tahun;
    protected $semester;

    function __construct($tahun, $semester) {
        $this->tahun = $tahun;
        $this->semester = $semester;
    }
    
    public function query()
        {
            $data = DB::connection('dbmoodle')
            ->table('enroll_krs_dosen')
            ->select('input_type', 'type', 'npp', 'shortname')
            ->where('tahun_akademik', '=', $this->tahun)
            ->where('semester', '=', $this->semester)
            ->orderBy('input_type');
            
            return $data;
        }

    public function registerEvents(): array
        {
            return [
                AfterSheet::class    => function(AfterSheet $event) {
                    $cellRange = 'A1:W1'; // All headers
                    $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);

                    $event->sheet->getDelegate()->getStyle('A1:P1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getStyle('A1:P5000')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                },
            ];
        }
}