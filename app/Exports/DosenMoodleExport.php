<?php

namespace App\Exports;

use App\Models\DosenMoodle;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class DosenMoodleExport implements FromQuery, WithHeadings, WithStrictNullComparison, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function query()
        {
            $data = DB::connection('dbmoodle')
            ->table('dosen')
            ->select('username', 'firstname', 'lastname', 'email', 'idnumber', 'auth')
            ->whereNotNull('email')
            ->orderBy('username');

            return $data;
        }

    public function registerEvents(): array
        {
            return [
                AfterSheet::class    => function(AfterSheet $event) {
                    $cellRange = 'A1:W1'; // All headers
                },
            ];
        }

    public function headings(): array
        {
            return [
                [
                    'username',
                    'firstname',
                    'lastname',
                    'email',
                    'idnumber',
                    'auth',
                ]
            ];
        }
}
