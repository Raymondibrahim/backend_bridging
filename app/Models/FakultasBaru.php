<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FakultasBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_fakultas';
    protected $table = 'fakultas';
    protected $fillable = ['id_fakultas', 'nama_fakultas', 'alamat_fakultas', 'id_fakultas_simka'];
}
