<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MahasiswaBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'idnumber';
    protected $table = 'kuliah_user';
    protected $fillable = ['username', 'idnumber', 'firstname', 'lastname', 
    'address', 'auth', 'email', 'city', 'country', 'lang', 'timezone',
    'timecreated', 'timemodified', 'confirmed'];
}
