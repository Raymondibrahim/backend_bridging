<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdiBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_prodi';
    protected $table = 'prodi';
    protected $fillable = ['id_prodi', 'id_fakultas', 'nama_prodi', 'jenjang'];
}
