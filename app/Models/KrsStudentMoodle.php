<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KrsStudentMoodle extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_krs';
    protected $table = 'enroll_krs_student';
}
