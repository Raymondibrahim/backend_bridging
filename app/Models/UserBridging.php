<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBridging extends Model
{
    use HasFactory;

    protected $connection = 'dbmoodle';
    public $timestamps = true;
    protected  $primaryKey = 'id';
    protected $table = 'users';

    protected $fillable = [
        'name',
        'prodi',
        'username',
        'password',
        'created_at',
        'updated_at',
    ];

    public function getCreatedAttribute(){
        if(!is_null($this->attributes['created_at'])){
            return Carbon::parse($this->attributes['created_at'])->format('Y-m-d H:i:s');
        }
    } //convert attribute created_at ke format T-m-d H:i:s

    public function getUpdatedAttribute(){
        if(!is_null($this->attributes['updated_at'])){
            return Carbon::parse($this->attributes['updated_at'])->format('Y-m-d H:i:s');
        }
    } //convert attribute created_at ke format Y-m-d H:i:s
}
