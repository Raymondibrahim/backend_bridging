<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KrsDosenBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_krs';
    protected $table = 'enroll_krs_dosen';
    protected $fillable = ['id_kelas', 'npp', 'fullname', 'shortname', 'kode_mk', 'nama_mk',
                            'nama_mk_eng', 'kelas', 'id_prodi', 'smak', 'tahun_akademik', 'semester',
                        'npp_dosen1', 'npp_dosen2', 'npp_dosen3', 'npp_dosen4', 'aka', 'sks'];
}
