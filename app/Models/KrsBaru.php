<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KrsBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_krs';
    protected $table = 'enroll_krs_student';
    protected $fillable = ['id_krs', 'npm', 'shortname', 'tglkrs',
                            'kode_mk', 'kelas', 'id_prodi', 'smak', 'tahun_akademik', 'semester'];
}
