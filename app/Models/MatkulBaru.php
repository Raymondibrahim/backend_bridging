<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatkulBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected  $primaryKey = 'id_fakultas';
    protected $table = 'matakuliah';
    protected $fillable = ['id_kelas', 'shortname', 'fullname', 'category_path',
                            'idnumber', 'format', 'summary', 'id_prodi'];
}