<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmakBaru extends Model
{
    use HasFactory;
    protected $connection = 'dbmoodle';
    public $timestamps = false;
    protected $table = 'smak';
    protected $fillable = ['ID_TAHUN_AKADEMIK', 'NO_SEMESTER', 'SEMESTER_AKADEMIK', 'ISCURRENT'];
}
