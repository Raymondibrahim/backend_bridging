<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route::group(['middleware' => 'auth:api'],function(){
    Route::get('user','UserController@index');
    Route::get('user/{id}','UserController@show');
    Route::post('user','UserController@store');
    Route::put('user/{id}','UserController@update');
    Route::delete('user/{id}','UserController@destroy');

    Route::post('mhs_siatma', 'Siatma\MahasiswaSiatmaController@index');
    Route::post('mhs_baru', 'Baru\MahasiswaBaruController@index');
    Route::post('insertmhs', 'Baru\MahasiswaBaruController@store');
    Route::get('mhs_moodle', 'Moodle\MahasiswaMoodleController@index');

    Route::get('prodi_siatma', 'Siatma\ProdiSiatmaController@index');
    Route::get('fakultas_siatma', 'Siatma\FakultasSiatmaController@index');
    Route::get('smaksi', 'Siatma\SmakSiatmaController@index');
    Route::post('matkul_siatma', 'Siatma\MatkulSiatmaController@index');
    Route::get('mhs_clean', 'CleanDataMhsController@index');

    Route::get('dosen_siatma', 'Siatma\DosenSiatmaController@index');
    Route::get('dosen_baru', 'Baru\DosenBaruController@index');
    Route::post('insertdsn', 'Baru\DosenBaruController@store');
    Route::get('dsn_moodle', 'Moodle\DosenMoodleController@index');
    Route::post('updateemail/{id}', 'Moodle\DosenMoodleController@update');
    

    Route::get('prodi_baru', 'Baru\ProdiBaruController@index');
    Route::post('insertprodi', 'Baru\ProdiBaruController@store');

    Route::get('fakultas_baru', 'Baru\FakultasBaruController@index');
    Route::post('insertfakultas', 'Baru\FakultasBaruController@store');

    Route::post('matkul_baru', 'Baru\MatkulBaruController@index');
    Route::post('insertmatkul', 'Baru\MatkulBaruController@store');
    Route::post('matkul_moodle', 'Moodle\MatkulMoodleController@index');

    Route::get('smak_baru', 'Baru\SmakBaruController@index');
    Route::post('insertsmak', 'Baru\SmakBaruController@store');

    Route::post('krs_siatma', 'Siatma\KrsSiatmaController@index');
    Route::post('krs_baru', 'Baru\KrsBaruController@index');
    Route::post('insertkrs_student', 'Baru\KrsBaruController@store');
    Route::get('krs_moodle', 'Moodle\KrsMoodleController@index');

    Route::post('krs_dosen_baru', 'Baru\KrsDosenBaruController@index');
    Route::post('insertkrs_dosen', 'Baru\KrsDosenBaruController@store');
    Route::post('krs_dosen_moodle', 'Moodle\KrsDosenMoodleController@index');
    Route::post('insertkrs_dosenmanual', 'Baru\KrsDosenBaruController@insert');

    Route::post('logout', 'AuthController@logout');
//});

Route::post('login', 'AuthController@login');
Route::get('export_krs_student', 'KrsStudentMoodleExportController@export')->name('drc.export');
Route::get('export_krs_dosen', 'KrsDosenMoodleExportController@export')->name('drc.export');
Route::get('export_maba', 'MahasiswaMoodleExportController@export')->name('drc.export');
Route::get('export_dosen', 'DosenMoodleExportController@export')->name('drc.export');
Route::get('export_matkul', 'MatkulMoodleExportController@export')->name('drc.export');

